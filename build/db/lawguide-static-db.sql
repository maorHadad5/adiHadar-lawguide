insert into lawguide_db.lawguide_app_lawyer
(name, location,phone_number,free_text_about_the_lawyer,office_name) values
('חן דיין', 'אשקלון','056-9930917','המשרד הוקם בשנת 2010 וצבר ניסיון רב בתחומו, נותן את השירות הטוב ביותר','חן ושות'),
('עדי הדר', 'נתיבות','050-9930916','המשרד הוקם בשנת 2010 וצבר ניסיון רב בתחומו, נותן את השירות הטוב ביותר','עדי ושות'),
('מאור חדד', 'מעגלים','056-9930917','המשרד הוקם בשנת 2010 וצבר ניסיון רב בתחומו, נותן את השירות הטוב ביותר','מאור ושות');

insert into lawguide_db.lawguide_app_user
(name) values
('מאור'),
('עדי');

insert into lawguide_db.lawguide_app_occupation
(occupation) values
('תעבורה'),
('נזיקין');

INSERT INTO lawguide_db.lawguide_app_occupation_id_lawyer
(occupation_id, lawyer_id) values
('1', '1'),
('1', '2'),
('1', '3'),
('2', '3');

insert into lawguide_db.lawguide_app_hours
(hour) values
('10:00'),
('11:00'),
('12:00'),
('13:00'),
('14:00'),
('15:00'),
('16:00');

insert into lawguide_db.lawguide_app_recommendations
(picture, text) values
('https://bennygamzo.com/wp-content/uploads/2019/02/%D7%A6%D7%99%D7%9C%D7%95%D7%9D-%D7%AA%D7%93%D7%9E%D7%99%D7%AA-%D7%9C%D7%A2%D7%95%D7%A8%D7%9B%D7%99-%D7%93%D7%99%D7%9F3-1-1024x682.jpg','מתחילת הדרך עו”ד מאור התווה אסטרטגיה ברורה, מדויקת עם לוח זמנים מפורט.
אני ממליצה מאוד על עו”ד מאור, הוא הגון, ברור מאוד, אפקטיבי – ומגיע לתוצאות מצוינות  בזמן קצר !!!'),
('https://lookbetterphoto.co.il/wp-content/uploads/2020/01/040_%D7%A6%D7%99%D7%9C%D7%95%D7%9D-%D7%AA%D7%9E%D7%95%D7%A0%D7%AA-%D7%A4%D7%A8%D7%95%D7%A4%D7%99%D7%9C-%D7%9C%D7%90%D7%AA%D7%A8-%D7%94%D7%9B%D7%A8%D7%95%D7%99%D7%95%D7%AA-800x1200.jpg','אני רוצה להודות לעו"ד דניאל על הלווי שנתת לי עד לפתרון שהיה לשביעות רצוני המלאה.
אני ממליצה בחום על עו”ד עדי כבעל מקצוע , אמין, מקצועי, הגון, עם ניסיון רב במיוחד בתחום התעבורה.'),
('https://www.meshek8.co.il/wp-content/uploads/2016/01/%D7%A6%D7%99%D7%9C%D7%95%D7%9D_%D7%AA%D7%9E%D7%95%D7%A0%D7%AA_%D7%A4%D7%A8%D7%95%D7%A4%D7%99%D7%99%D7%9C_%D7%A8%D7%99%D7%99%D7%A6%D7%B3%D7%9C.jpg','פניתי לעו"ד חן ומהר מאוד נוכחתי לדעת שהוא מקצועי מאוד, נכנס לכל הפרטים ובעל ניסיון רב.');

INSERT INTO lawguide_db.lawguide_app_videorooms
(url, taken) values
('https://proguide.daily.co/AQ3l7y8TGBhyvG5X2x7l', 0),
('https://proguide.daily.co/hfy4w6zrweSShgfDMgle', 0),
('https://proguide.daily.co/7FOdveevIBxaAsgmipbX', 0),
('https://proguide.daily.co/4buBlFHjykBG8uGf6tXd', 0),
('https://proguide.daily.co/pvGQKVhn2AEGIJ3b4iBX', 0);