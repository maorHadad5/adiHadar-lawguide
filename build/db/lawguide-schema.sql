CREATE DATABASE IF NOT EXISTS `lawguide_db`;


DROP USER IF EXISTS 'lawguide_db_user'@'%' ;
-- password MD5('lawguide_db_user')
CREATE USER 'lawguide_db_user'@'%' IDENTIFIED WITH mysql_native_password BY '5ee7fe6c07d6dccb235f54f4f7109a62';
GRANT ALL ON lawguide_db.* TO 'lawguide_db_user'@'%';
FLUSH PRIVILEGES;