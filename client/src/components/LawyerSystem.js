import React, {useEffect, useState} from 'react';
import service from "../services/api";
import './LawyerSystem.css'
import {Button, Table, Container, Row, Col, Form} from "react-bootstrap";

function LawyerSystem() {
    const [meetings_waiting_to_confirm, set_meetings_waiting_to_confirm] = useState([]);
    const [meetings_confirmed, set_meetings_confirmed] = useState([]);
    const [lawyer_ids, set_lawyer_ids] = useState([]);
    const [users, set_users] = useState([]);

    useEffect(() => {
        if (lawyer_ids.length === 0) {
            service.Api_guide.GetLawyers()
                .then(respond => {
                        set_lawyer_ids(respond.data)
                    }
                )
        }
        if (users.length === 0) {
            service.Api_guide.get_all_users().then(respond => {
                set_users(respond.data)
            })

        }
    });

    function get_user_by_id(user_id) {
        let name_to_ret = ""

        users.map(user => {
            if (user.pk === user_id) {
                name_to_ret = user.fields.name
            }
        })
        return name_to_ret

    }

    function get_the_waiting_for_lawyer_confirm_meetings(lawyer_id) {

        if (lawyer_id === "") {
            alert("חובה להכניס זהות העורך דין!")
        } else {
            service.Api_guide.get_the_waiting_for_lawyer_confirm_meetings(lawyer_id)
                .then(respond => {
                    set_meetings_waiting_to_confirm(respond.data)
                    if (respond.data.length === 0) {
                        document.getElementById('meetings_wait').innerHTML = "אין פגישות ממתינות!  "
                        document.getElementById('no_meetings_wait').innerHTML = ""


                    } else {
                        document.getElementById('meetings_wait').innerHTML = "פגישות ממתינות: "
                        document.getElementById('no_meetings_wait').innerHTML = ""


                    }
                })
            service.Api_guide.get_confirmed_meetings(lawyer_id)
                .then(respond_confirmed => {
                    set_meetings_confirmed(respond_confirmed.data)
                    if (respond_confirmed.data.length === 0) {
                        document.getElementById('no_meetings_confirmed').innerHTML = "אין פגישות מאושרות! "
                        document.getElementById('meetings_confirmed_to_show').innerHTML = ""

                    } else {
                        document.getElementById('meetings_confirmed_to_show').innerHTML = "פגישות שאישרת: "
                        document.getElementById('no_meetings_confirmed').innerHTML = ""

                    }
                })


        }


    }


    const confirm_meeting = (meeting) => {
        service.Api_guide.confirm_meeting(meeting.pk)
            .then(respond => {
                alert(respond)
            })
        // console.log(meetings_waiting_to_confirm)
        //     service.Api_guide.get_the_waiting_for_lawyer_confirm_meetings(lawyer_id)
        //         .then(respond => {
        //             set_meetings_waiting_to_confirm(respond.data)
        //             if (respond.data.length === 0) {

        //
        //
        //             } else {
        //                 document.getElementById('meetings_wait').innerHTML = "פגישות ממתינות: "
        //                 document.getElementById('no_meetings_wait').innerHTML = ""
        //
        //
        //             }
        //         }
        //         )
        // console.log(meetings_waiting_to_confirm)
        //
        //
        // service.Api_guide.get_confirmed_meetings(lawyer_id)
        //     .then(respond_confirmed => {
        //         set_meetings_confirmed(respond_confirmed.data)
        //         if (respond_confirmed.data.length === 0) {
        //             document.getElementById('no_meetings_confirmed').innerHTML = "אין פגישות מאושרות! "
        //             document.getElementById('meetings_confirmed_to_show').innerHTML = ""
        //
        //         } else {
                    document.getElementById('meetings_confirmed_to_show').innerHTML = "פגישות שאישרת: "
                    document.getElementById('no_meetings_confirmed').innerHTML = ""
        //
        //         }
        //     })
        let flag_been_in_push=false
        let waiting = []
        let approved = []
        meetings_waiting_to_confirm.map(meet => {
            if (meet.pk !== meeting.pk) {
                waiting.push(meet)
            }
        })
        set_meetings_waiting_to_confirm(waiting)
        if (meetings_confirmed.length === 0) {
            document.getElementById('meetings_confirmed_to_show').innerHTML = "פגישות שאישרת: "
            document.getElementById('no_meetings_confirmed').innerHTML = ""
            meetings_confirmed.push(meeting)
        } else {

            meetings_confirmed.map(meet => {
                if (parseInt(meet.fields.hour.charAt(0)+meet.fields.hour.charAt(1)) > parseInt(meeting.fields.hour.charAt(0)+meeting.fields.hour.charAt(1))) {
                  flag_been_in_push=true
                    approved.push(meeting)
                    console.log("int",parseInt(meet.fields.hour.charAt(0)+meet.fields.hour.charAt(1)) )
                }
                approved.push(meet)
                // console.log(meet.fields.hour)


            })
            if(!flag_been_in_push){
                approved.push(meeting)
            }
            set_meetings_confirmed(approved)
        }

        console.log(meetings_waiting_to_confirm.length)
        if (meetings_waiting_to_confirm.length === 1) {
            document.getElementById('meetings_wait').innerHTML = "אין פגישות ממתינות!  "
            document.getElementById('no_meetings_wait').innerHTML = ""
        }


    }
    return (
        <div className='form_id'>

            <form className={'form_lawyer'}>
                <h1>עורך דין</h1>
                <label htmlFor="id">הכנס את מספר הזהות שלך:</label>&nbsp;&nbsp;

                <input required className={'input'} type="number" min={'1'} max={lawyer_ids.length} id="id"
                       name="id"
                /><br/><br/>
                <Button variant="primary"
                        onClick={() => get_the_waiting_for_lawyer_confirm_meetings(document.getElementById("id").value)}
                >צפה בפגישות
                </Button>

            </form>


            <div className='waitingMeet'>

                <h1 id={'meetings_wait'}></h1>
                <h2 id={'no_meetings_wait'}></h2>
                <Table className={'waiting_meetings_table'} striped bordered hover variant="dark">
                    {
                        meetings_waiting_to_confirm.map((item, index) => {
                            if (index === 0) {
                                return (<thead>
                                <tr>

                                    <th>משתמש</th>
                                    <th>שעה</th>
                                    <th>פעולה</th>
                                </tr>
                                </thead>)
                            }
                        })
                    }

                    {
                        meetings_waiting_to_confirm.map((meet, index) => {

                                return (

                                    <tbody>
                                    <tr>
                                        <td>{get_user_by_id(meet.fields.user)}</td>
                                        <td>{meet.fields.hour}</td>
                                        <td>
                                            <Button variant="primary"
                                                    onClick={() => confirm_meeting(meet)}>אשר
                                            </Button>
                                        </td>

                                    </tr>

                                    </tbody>


                                )
                            }
                        )
                    }
                </Table>

            </div>


            <div className='confirmedMeet'>

                <h1 className={'confirmed'} id={'meetings_confirmed_to_show'}></h1>
                <h2 className={'confirmed'} id={'no_meetings_confirmed'}></h2>
                <Table className={'approved_meetings_table'} striped bordered hover variant="dark">
                    {
                        meetings_confirmed.map((item, index) => {
                            if (index === 0) {
                                return (<thead>
                                <tr>

                                    <th>משתמש</th>
                                    <th>שעה</th>
                                    <th>לינק לפגישה</th>
                                </tr>
                                </thead>)
                            }
                        })
                    }

                    {

                        meetings_confirmed.map((meet, index) => {


                                return (

                                    <tbody>

                                    <tr>

                                        <td>{get_user_by_id(meet.fields.user)}</td>
                                        <td>{meet.fields.hour}</td>
                                        <td><Button variant="primary"><a className={'link_button'}
                                                                         href={"/Video-Room?id=" + meet.fields.user}> לינק
                                            לפגישה</a></Button></td>
                                    </tr>

                                    </tbody>


                                )
                            }
                        )
                    }
                </Table>

            </div>
        </div>
    )
        ;
}

export default LawyerSystem;


