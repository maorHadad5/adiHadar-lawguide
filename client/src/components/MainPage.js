import React from 'react';
import './MainPage.css'
import service from "../services/api";
import {Button, Card, Col, Row} from "react-bootstrap";

export class MainPage extends React.Component {
    state = {
        recommendations: []
    }

    componentDidMount() {
        service.Api_guide.GetRecommendations()
            .then(respond => {
                this.setState({
                    recommendations: respond.data
                });
                console.log(respond.data)
                return respond;
            })
    }

    render() {
        function start_system() {
            console.log(6)
            this.props.history.push({
                pathname: '/search-page',

            })
        }

        return (

            <div className={"main_page"}>

                <a href="/search-page"> <Button onClick={()=>start_system()} className={"button_start"}> התחל בתהליך</Button></a>
                <h1 className={'proguide_title'}>proguide</h1>
            <Row >
                {

                    this.state.recommendations.map(item => {
                        return(<Col>
                            <Card className={'cards_req'}>
                            <Card.Img className={'pic_cards'} variant="top" src={item.fields.picture} />
                            <Card.Body>
                                <Card.Text >
                                    {item.fields.text}

                                </Card.Text>

                            </Card.Body>
                        </Card>
                            </Col>
                        )
                    }

                    )
                }
            </Row>
            </div>
        );
    }
}
// <div className="responsive">
//     <div className="gallery">
//         <img src={item.fields.picture} alt="pic" width="600" height="400"/>
//         <div className="desc">{item.fields.text}</div>
//     </div>
// </div>