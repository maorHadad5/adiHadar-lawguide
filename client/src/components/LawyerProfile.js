import React from 'react';
import './LawyerProfile.css'
import lawyer_pic from "../images/lawyer-pic.png";
import {ListGroupItem,ListGroup,Card,Button, Container, Row, Table, Col} from "react-bootstrap";


function LawyerProfile({lawyer}) {
    // console.log(lawyer)
    return (
        <div className={'full_page'} >
<Container className={'profile'} >
        <Card className={'card1'} style={{     backgroundColor: "#e0eec7"}} >
            <Card.Img className={'pic_card'} variant="top" src={lawyer_pic} />
            <Card.Body>
                <Card.Title >{lawyer.name}</Card.Title>
            </Card.Body>
            <ListGroup className="list-group-flush" >
                <ListGroupItem>{lawyer.location}</ListGroupItem>
                <ListGroupItem>{lawyer.office_name}</ListGroupItem>
                <ListGroupItem>{lawyer.phone_number}</ListGroupItem>
                <ListGroupItem>{lawyer.free_text_about_the_lawyer}</ListGroupItem>
            </ListGroup>

        </Card>

</Container>
        </div>
    );
}

export default LawyerProfile;
