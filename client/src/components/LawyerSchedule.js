import React, {useEffect, useState} from 'react';
import service from "../services/api";
import "./LawyerSchedule.css"
import {ListGroupItem, ListGroup, Card, Button, Container, Row, Table, Col} from "react-bootstrap";
import * as ReactBootstrap from 'react-bootstrap';

const LawyerSchedule = ({lawyer}) => {

    const [free_hours, set_free_hours] = useState([]);
    const [meetings, set_meetings] = useState([]);

    useEffect(() => {
        if (meetings.length === 0 && free_hours.length === 0) {
            service.Api_guide.get_all_meetings_of_lawyer(lawyer.pk)
                .then(respond => {
                    set_meetings(respond.data)
                })
            service.Api_guide.GetAllFreeHoursOfLawyer(lawyer.pk)
                .then(respond => {
                    set_free_hours(respond.data)
                })
        }
    });
    const add_meeting = (hour) => {
        let flag_waiting = false;
        let flag_confirmed = false;

        meetings.forEach(meeting => {
            if (meeting.waiting_for_lawyer_confirm === true && meeting.hour === hour) {
                flag_waiting = true;
                alert("already pressed - waiting for lawyer confirm ")
            } else {
                if (meeting.confirmed === true && meeting.hour === hour) {
                    flag_confirmed = true;
                    alert("already pressed - lawyer confirmed this meeting ")
                }
            }
        })
        if (flag_confirmed === false && flag_waiting === false) {
            service.Api_guide.addMeeting(hour, lawyer.pk, 1, true, false)
                .then(respond => alert(respond))
        }
        window.location.reload();
    }

    function set_state(hour) {
        if (meetings.length === 0) {
            return "פנוי"
        }
        for (let i = 0; i < meetings.length; i++) {
            if (meetings[i].hour === hour) {
                if (meetings[i].confirmed) {
                    return "אושר"
                } else {
                    return "ממתין לאישור העו''ד"
                }
            }
        }
        for (let i = 0; i < meetings.length; i++) {
            if (meetings[i].hour !== hour) {
                return "פנוי"
            }
        }
    }

    function refresh() {
        window.location.reload();

    }

    return (


      <div className={'all_page'}>
            <h1 className={'title_schedule'}>לוח זמני עורך הדין:</h1>
    <div className={'f'}>
            <Table striped bordered hover variant="dark">
                <thead>
                <tr>

                    <th>שעה</th>
                    <th>פעולה</th>
                    <th>מצב</th>
                </tr>
                </thead>
                <tbody>
                {
                    free_hours.map(hour => {
                            if (hour === "אין פגישות פנויות עבור עורך דין זה") {
                                return <li key={hour}>{hour} </li>
                            } else {
                                return (<tr key={hour}>
                                        <td>{hour}</td>
                                        <td>
                                            <Button className={"set_meeting_button"} onClick={() => add_meeting(hour)}>קבע
                                                פגישה
                                            </Button>
                                        </td>
                                        <td>
                                            <div>{set_state(hour)}</div>
                                        </td>
                                    </tr>
                                )
                            }
                        }
                    )

                }

                </tbody>
            </Table>
        </div>
      </div>
    )
}
export default LawyerSchedule;
