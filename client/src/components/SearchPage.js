import React from 'react';
import service from "../services/api";
import './SearchPage.css'
import {ListGroupItem, ListGroup, Card, Button, Container, Row, Table, Col} from "react-bootstrap";

export class SearchPage extends React.Component {

    state = {
        lawyers: [],
        occupations: []
    }

    show_lawyers(occupation) {
        service.Api_guide.GetLawyersByOccupation(occupation)
            .then(respond => {
                this.setState({
                    lawyers: respond.data
                });
                return respond;
            })
    }

    componentDidMount() {
        this.show_occupation();
        this.show_init_lawyers_tables();
    }

    show_occupation() {
        service.Api_guide.GetAllOccupations()
            .then(respond => {
                // console.log("the",respond)
                this.setState({
                    occupations: respond.data
                });
                return respond;
            })
    }

    // show_lawyer_page(lawyer) {
    //     return(  <a className={'link_button'} href={"/Video-Room?id=" + meet.fields.user}> לינק לפגישה</a>
    //     )
    //     // console.log(lawyer)
    //     // this.props.history.push({
    //     //     pathname: '/Lawyer-Page',
    //     //     state: {detail: lawyer}
    //     // })
    // }

    show_init_lawyers_tables() {
        service.Api_guide.GetLawyers()
            .then(respond => {
                // console.log("the",respond)
                this.setState({
                    lawyers: respond.data
                });
                return respond;
            })
    }

    render() {
        return (
            <div className={'search_lawyer_full_page'}>
                <div className={'all_element'}>
                    <p className="text"> סוגי התמחויות: </p>
                    <div className="occupation-buttons">
                        {
                            this.state.occupations.map(item => (
                                <button className="button" key={item.fields.occupation}
                                        onClick={() => this.show_lawyers(item.fields.occupation)}>
                                    <span>{item.fields.occupation}</span></button>
                            ))
                        }
                    </div>


                    <Table className={'search_table'} striped bordered hover
                           style={{width:"50%",color: "white", backgroundColor: "#4CAF50"}}>
                        <thead>
                        <tr>
                            <th key={"name"}>שם ושם משפחה</th>
                            <th key={"location"}>מיקום</th>
                            <th key={"appointment"}>קביעת פגישה</th>
                        </tr>
                        </thead>
                        <tbody>
                        {
                            this.state.lawyers.map(item => {
                                return (<tr>
                                    <td key={item.fields.name}>{item.fields.name}</td>
                                    <td key={item.fields.location}>{item.fields.location}</td>
                                    <td key={item.pk}>
                                        <Button variant="primary">
                                            <a className={'link_button'} href={"/Lawyer-Page?lawyer_id="+item.pk}>לחץ</a>
                                        </Button>
                                    </td>
                                </tr>)
                            })
                        }


                        </tbody>
                    </Table>
                </div>

            </div>
        );
    }


}