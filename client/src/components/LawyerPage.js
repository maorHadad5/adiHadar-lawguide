import LawyerSchedule from "./LawyerSchedule";
import './LawyerPage.css'

import React from 'react';
// import service from "../services/api";
import LawyerProfile from "./LawyerProfile";

import {Button, Container, Row, Table, Col} from "react-bootstrap";
import service from "../services/api";

export class LawyerPage extends React.Component {
    state = {
        lawyer: [],

    }

    getUrlParam(parm) {
        return new URLSearchParams(window.location.search).get(parm)
    }

    componentDidMount() {
        if (this.state.lawyer.length === 0) {
            let lawyer_id = this.getUrlParam("lawyer_id")
            console.log(lawyer_id)
            service.Api_guide.get_lawyer_by_id(lawyer_id).then(respond => {
                this.setState({
                    lawyer: respond.data
                });

            })
        }
    }

    render() {


        return (
            <Container>
                <div className={'full_page'}>
                    {
                        this.state.lawyer.map(lawyer => {
                            return (
                                <div key={lawyer.pk}>
                                    <LawyerProfile lawyer={lawyer.fields}/>
                                    <LawyerSchedule lawyer={lawyer}/>
                                </div>
                            )
                        })
                    }

                </div>


            </Container>

        )
    }
}