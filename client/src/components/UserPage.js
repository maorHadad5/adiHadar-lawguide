import React, {useEffect, useState} from 'react';
import "./UserPage.css"
import service from "../services/api";
import {ListGroupItem, ListGroup, Card, Button, Container, Row, Table, Col} from "react-bootstrap";

function UserPage() {
    const [user_all_meetings, set_user_all_meetings] = useState([]);
    const [lawyers, set_lawyers] = useState([]);

    let index = 0;
    useEffect(() => {
        if (user_all_meetings.length === 0) {
            service.Api_guide.get_all_meetings_of_user(1)
                .then(respond => {
                    set_user_all_meetings(respond.data)
                    if (respond.data.length === 0) {
                        document.getElementById('no_meetings').innerHTML = "לא קיימות פגישות:  "
                    } else {
                        document.getElementById('meetings_to_show').innerHTML = "הפגישות שלך:  "
                        document.getElementById('no_meetings').innerHTML = ""

                    }
                })
        }
        if (lawyers.length === 0) {
            service.Api_guide.GetLawyers().then(respond => {
                set_lawyers(respond.data)
                console.log(respond.data)
            })
        }
    });

    function get_lawyer_by_id(id_lawyer) {
        let name_to_ret = ""
        lawyers.map(lawyer => {
            if (lawyer.pk === id_lawyer) {
                name_to_ret = lawyer.fields.name
            }
        })
        return name_to_ret

    }

    return (<div className={'full_user_page'}>
            <div className={'user_meetings'}>
            <h1 className={'user_meeting_title'} id={'meetings_to_show'}></h1>
            <h2 id={'no_meetings'}></h2>
            <Table className={'user_meeting_table'}   style={{width:"100%"}} striped bordered hover variant="dark">
                {
                    user_all_meetings.map((item, index) => {
                        if (index === 0) {
                            return (<thead>
                            <tr>
                                <th>עורך דין</th>
                                <th>שעה</th>
                                <th>לינק לפגישה</th>

                            </tr>
                            </thead>)
                        }
                    })
                }

                {

                    user_all_meetings.map(meet => {


                            return (

                                <tbody>
                                <tr>
                                    <td>{get_lawyer_by_id(meet.lawyer)}</td>
                                    <td>{meet.hour}</td>
                                    <td><a className="active" href="/Video-Room?id=1">לינק לפגישה</a></td>
                                </tr>

                                </tbody>


                            )


                        }
                    )

                }
            </Table>
            </div>
        </div>


    );
}
export default UserPage;