import React from 'react';
import './Header.css';
import logo from '../../images/logo.png';

function Header() {
    // Import result is the URL of your image
    return (<div className="header">
            <img src={logo} alt="Logo" width="12%" height="8%"/>
        </div>
    );
}
export default Header;

