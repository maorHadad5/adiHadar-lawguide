import React from 'react';
import './NavigationBar.css';
import logo from '../../images/logo.png';
import  {Nav,Navbar,Container} from "react-bootstrap";

const NavigationBar=() =>{
    return (

        <ul className={'navigate'}>
            <li><a  href="/Main-Page">דף הבית</a></li>
            <li><a className="active" href="/User-Page">פרופיל - משתמש </a></li>
            <li><a className="active" href="/Lawyer-System">פרופיל - עו''ד </a></li>
            {/*<li><a href="/">User</a></li>*/}
            <li><img  className={"logo1"} src={logo} alt="Logo" width="220" height="60"/></li>

        </ul>


        // <Navbar  bg={'light'} variant={'light'} >
        //
        //         <Navbar.Brand href="#home">Navbar</Navbar.Brand>
        //         <Nav className="me-auto">
        //             <Nav.Link href="#home">Home</Nav.Link>
        //             <Nav.Link href="#features">Features</Nav.Link>
        //             <Nav.Link href="#pricing">Pricing</Nav.Link>
        //         </Nav>
        //
        // </Navbar>



    )
}

export default NavigationBar;


