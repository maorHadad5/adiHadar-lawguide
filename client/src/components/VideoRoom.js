import React from 'react';
import service from "../services/api";
import DailyRoom from "../components-daily/DailyRoom/DailyRoom";

export class VideoRoom extends React.Component {

    state = {
        room: null
    }

    componentDidMount() {

        const room_id = this.getUrlParam("id");
        if(room_id) {
            service.Api_guide.get_url_by_id_room(room_id)
                .then(respond => {
                     console.log("the", respond.data[0].fields.url)
                    this.setState({
                        room: respond.data[0].fields.url

                    });
                    return respond;
                })
        } else{
            this.setState({
                room: this.getUrlParam("roomUrl")
            });
        }
    }

    getUrlParam(parm) {
        return new URLSearchParams(window.location.search).get(parm)
    }

    show_lawyer_page(lawyer) {
        // console.log(lawyer)
        this.props.history.push({
            pathname: '/Lawyer-Page',
            state: {detail: lawyer}
        })
    }

    render() {
        return this.state.room ?
            (<DailyRoom room_url={this.state.room} />)
            : (
                <div>טוען...</div>
            );
    }


}