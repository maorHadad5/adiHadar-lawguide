
import Axios from 'axios'

const $axios = Axios.create({
    baseURL: '/api/',
    headers: {
        'Content-Type': 'application/json'
    }
})
$axios.interceptors.response.use(
    (response) => response,
    (error) => {
        console.error("got error")
        console.error(error)

        throw error;
    });

class Api_guide {


    static GetLawyers() {
        return $axios
            .get('get_all_lawyers')
            .then(response => {
                return response;
            })
    }
    static get_all_users() {
        return $axios
            .get('get_all_users')
            .then(response => {
                return response;
            })
    }

    static GetRecommendations() {
        return $axios
            .get('get_all_recommendations')
            .then(response => {
                return response;
            })
    }
    static GetAllOccupations() {
        return $axios
            .get('get_all_occupations')
            .then(response => {
                return response;
            })
    }

    static GetLawyersByOccupation(occupation) {
        return $axios
            .get(`get_lawyers_by_occupation/${occupation}`)
            .then(response => {
                return response;
            })
    }
    static GetAllFreeHoursOfLawyer(id_lawyer) {
        return $axios
            .get(`get_all_free_hours_of_lawyer/${id_lawyer}`)
            .then(response => {
                return response;
            })
    }
    static get_user_by_id(id_user) {
        return $axios
            .get(`get_user_by_id/${id_user}`)
            .then(response => {
                return response;
            })
    }
    static get_lawyer_by_id(id_lawyer) {
        return $axios
            .get(`get_lawyer_by_id/${id_lawyer}`)
            .then(response => {
                return response;
            })
    }


    static get_confirmed_meetings(id_lawyer) {
        return $axios
            .get(`get_confirmed_meetings/${id_lawyer}`)
            .then(response => {
                return response;
            })
    }
    static get_url_by_id_room(id_room) {
        return $axios
            .get(`get_url_by_id_room/${id_room}`)
            .then(response => {
                return response;
            })
    }

    static get_the_waiting_for_lawyer_confirm_meetings(id_lawyer) {
        return $axios
            .get(`get_the_waiting_for_lawyer_confirm_meetings/${id_lawyer}`)
            .then(response => {
                return response;
            })
    }
    static addMeeting(hour, lawyer_id,user_id,waiting_for_lawyer_confirm,confirmed) {
        return $axios
            .post('add_meeting/', {hour, lawyer_id,user_id,waiting_for_lawyer_confirm,confirmed})
            .then(response => response.data)


    }
    static confirm_meeting(meeting_id) {
        return $axios
            .get(`confirm_meeting/${meeting_id}`)
            .then(response => response.data)
    }
    static get_all_meetings_of_lawyer(lawyer_id) {
        return $axios
            .get(`get_all_meetings_of_lawyer/${lawyer_id}`)
            .then(response => {
                return response;
            })
    }
    static get_all_meetings_of_user(user_id) {
        return $axios
            .get(`get_all_meetings_of_user/${user_id}`)
            .then(response => {
                return response;
            })
    }


}

const service = {
    Api_guide: Api_guide
}
export default service