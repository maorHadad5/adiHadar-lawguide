import React from 'react'
import {BrowserRouter as Router, Redirect, Route, Switch} from "react-router-dom";
import {SearchPage} from "./components/SearchPage";
import {LawyerPage} from "./components/LawyerPage";
import LawyerSchedule from "./components/LawyerSchedule";
import LawyerProfile from "./components/LawyerProfile";
import LawyerSystem from "./components/LawyerSystem";
import 'bootstrap/dist/css/bootstrap.min.css'
// import Header from "./components/general_component/Header";
import NavigationBar from "./components/general_component/NavigationBar";
// import Footer from "./components/general_component/Footer";
import {MainPage} from "./components/MainPage";
import UserPage from "./components/UserPage";
import {VideoRoom} from "./components/VideoRoom";
 import Background from "./components/general_component/Background";

const App = () => {
    function getUrlParam(parm) {
        return new URLSearchParams(window.location.search).get(parm)
    }

    const lawyer = getUrlParam("role");
    const id = getUrlParam("id");

    if (lawyer !== "lawyer") {
        return (
            <Router>
                <div>
                    {/*{/<Background/>/}*/}
                    {/*{/<Header/>/}*/}
                    <NavigationBar/>
                    <Background/>
                    <Switch>
                        <Route path="/Video-Room" component={VideoRoom}/>
                        <Route path="/User-Page" component={UserPage}/>
                        <Route path="/search-page" component={SearchPage}/>
                        <Route path="/Lawyer-Page" component={LawyerPage}/>
                        <Route path="/Lawyer-Schedule" component={LawyerSchedule}/>
                        <Route path="/Lawyer-System" component={LawyerSystem}/>
                        <Route path="/Lawyer-Profile" component={LawyerProfile}/>
                        <Route path="/Main-Page" component={MainPage}/>
                        <Route exact path="/"><Redirect to="/Main-Page"/>
                        </Route>
                    </Switch>
                    {/*<Footer/>*/}
                </div>
            </Router>
        )
    } else {
        return (
            <div>
                {/*{/<Header/>/}*/}
                <NavigationBar/>
                <LawyerSystem/>
                {/*<Footer/>*/}
            </div>
        )

    }

}
export default App;

