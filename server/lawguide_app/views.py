import json
from rest_framework import status
from rest_framework.decorators import api_view, renderer_classes
from rest_framework.renderers import JSONRenderer
from rest_framework.response import Response
from django.core import serializers
from .models import Occupation, Lawyer, Hours, Meetings, Recommendations, VideoRooms, User


@api_view(['GET'])
@renderer_classes([JSONRenderer])
def get_all_recommendations(request):
    all_recommendation_res = serializers.serialize('json', Recommendations.objects.all())
    json_all_rec_res = json.loads(all_recommendation_res)
    return Response(json_all_rec_res, status=status.HTTP_200_OK)


@api_view(['GET'])
@renderer_classes([JSONRenderer])
def get_lawyer_by_id(request, lawyerid):
    print(lawyerid)
    req_lawyer = Lawyer.objects.get(id=lawyerid)
    return Response(req_lawyer.id_lawyer, status=status.HTTP_200_OK)


@api_view(['GET'])
@renderer_classes([JSONRenderer])
def get_lawyer_by_location(request, locat):
    print(locat)
    req_location = Lawyer.objects.get(location=locat)
    return Response(req_location, status=status.HTTP_200_OK)


@api_view(['GET'])
@renderer_classes([JSONRenderer])
def get_lawyer_by_occupation(request, occupation):
    print(occupation)
    all_occupations = serializers.serialize('json', Occupation.objects.all())
    all_lawyers = serializers.serialize('json', Lawyer.objects.all())
    json_occupations = json.loads(all_occupations)
    json_lawyers = json.loads(all_lawyers)
    arr_dic_lawyers = []
    lawyer_ids = 0
    for i in json_occupations:
        if i['fields']['occupation'] == occupation:
            # lawyer_ids- save the array of lawyers ids from the fields id_lawyer
            lawyer_ids = i['fields']['id_lawyer']
    for i in json_lawyers:
        if i['pk'] in lawyer_ids:
            arr_dic_lawyers.append(i)
    return Response(arr_dic_lawyers, status=status.HTTP_200_OK)


@api_view(['GET'])
@renderer_classes([JSONRenderer])
def get_all_lawyers(request):
    all_lawyers_res = serializers.serialize('json', Lawyer.objects.all())
    json_all_lawyers_res = json.loads(all_lawyers_res)
    return Response(json_all_lawyers_res, status=status.HTTP_200_OK)


@api_view(['GET'])
@renderer_classes([JSONRenderer])
def get_all_users(request):
    all_users_res = serializers.serialize('json', User.objects.all())
    json_all_users_res = json.loads(all_users_res)
    return Response(json_all_users_res, status=status.HTTP_200_OK)




@api_view(['GET'])
@renderer_classes([JSONRenderer])
def get_all_occupations(request):
    all_occupations_res = serializers.serialize('json', Occupation.objects.all())
    json_all_occupations_res = json.loads(all_occupations_res)
    return Response(json_all_occupations_res, status=status.HTTP_200_OK)


@api_view(['GET'])
@renderer_classes([JSONRenderer])
def get_all_free_hours_of_lawyer(request, id_lawyer):
    all_hours = serializers.serialize('json', Hours.objects.all())
    json_all_hours_res = json.loads(all_hours)
    all_meetings = serializers.serialize('json', Meetings.objects.all())
    json_all_meetings_res = json.loads(all_meetings)

    all_hours_in_day = []
    all_busy_hours_of_this_lawyer = []
    all_free_hours_of_this_lawyer = []

    for hour_in_day in json_all_hours_res:
        all_hours_in_day.append(hour_in_day['fields']['hour'])

    # for meeting in json_all_meetings_res:
    #     if meeting['fields']['lawyer'] == id_lawyer:
    #         all_busy_hours_of_this_lawyer.append(meeting['fields']['hour'])
    #
    # for hour in all_hours_in_day:
    #     if hour not in all_busy_hours_of_this_lawyer:
    #         all_free_hours_of_this_lawyer.append(hour)
    # if len(all_free_hours_of_this_lawyer) == 0:
    #     return Response(["no available meetings of this lawyer"], status=status.HTTP_200_OK)
    return Response(all_hours_in_day, status=status.HTTP_200_OK)


@api_view(['GET'])
@renderer_classes([JSONRenderer])
def get_the_waiting_for_lawyer_confirm_meetings(request, lawyer_id):
    all_meetings = serializers.serialize('json', Meetings.objects.all())
    json_all_meetings_res = json.loads(all_meetings)
    all_meetings_of_this_lawyer = []
    for lawyer in json_all_meetings_res:
        if lawyer['fields']['lawyer'] == lawyer_id and lawyer['fields']['waiting_for_lawyer_confirm'] == True:
            all_meetings_of_this_lawyer.append(lawyer)
    return Response(all_meetings_of_this_lawyer, status=status.HTTP_200_OK)


@api_view(['GET'])
@renderer_classes([JSONRenderer])
def get_confirmed_meetings(request, lawyer_id):
    all_meetings = serializers.serialize('json', Meetings.objects.all())
    json_all_meetings_res = json.loads(all_meetings)
    all_confirmed_meetings_of_this_lawyer = []
    for lawyer in json_all_meetings_res:
        if lawyer['fields']['lawyer'] == lawyer_id and lawyer['fields']['confirmed'] == True:
            all_confirmed_meetings_of_this_lawyer.append(lawyer)
    return Response(all_confirmed_meetings_of_this_lawyer, status=status.HTTP_200_OK)



@api_view(['POST'])
@renderer_classes([JSONRenderer])
def add_meeting(request):
    data = request.data
    new_meeting = Meetings(hour=data['hour'], lawyer_id=data['lawyer_id'], user_id=data['user_id'],
                           waiting_for_lawyer_confirm=data['waiting_for_lawyer_confirm'],
                           confirmed=data['confirmed'])
    new_meeting.save()
    return Response("הפגישה נשלחה לאישור העורך דין ", status=status.HTTP_200_OK)


@api_view(['GET'])
@renderer_classes([JSONRenderer])
def confirm_meeting(request, meeting_id):
    meeting = Meetings.objects.get(id=meeting_id)
    meeting.confirmed = True
    meeting.waiting_for_lawyer_confirm = False
    meeting.save()
    return Response("approved!", status=status.HTTP_200_OK)


@api_view(['GET'])
@renderer_classes([JSONRenderer])
def get_all_meetings_of_lawyer(request, lawyer_id):
    all_meetings = serializers.serialize('json', Meetings.objects.all())
    json_all_meetings_res = json.loads(all_meetings)
    all_meetings_of_this_lawyer = []
    for lawyer in json_all_meetings_res:
        if lawyer['fields']['lawyer'] == lawyer_id:
            all_meetings_of_this_lawyer.append(lawyer['fields'])
    return Response(all_meetings_of_this_lawyer, status=status.HTTP_200_OK)


@api_view(['GET'])
@renderer_classes([JSONRenderer])
def get_url_by_id_room(request, id_room):
    all_rooms = serializers.serialize('json', [VideoRooms.objects.get(id=id_room)])
    json_all_rooms_res = json.loads(all_rooms)
    return Response(json_all_rooms_res, status=status.HTTP_200_OK)

@api_view(['GET'])
@renderer_classes([JSONRenderer])
def get_user_by_id(request, user_id):
    user = serializers.serialize('json', [User.objects.get(id=user_id)])
    json_user = json.loads(user)
    return Response(json_user, status=status.HTTP_200_OK)


@api_view(['GET'])
@renderer_classes([JSONRenderer])
def get_lawyer_by_id(request, lawyer_id):
    lawyer = serializers.serialize('json', [Lawyer.objects.get(id=lawyer_id)])
    json_lawyer = json.loads(lawyer)
    return Response(json_lawyer, status=status.HTTP_200_OK)


@api_view(['GET'])
@renderer_classes([JSONRenderer])
def get_all_meetings_of_user(request, user_id):
    all_meetings = serializers.serialize('json', Meetings.objects.all())
    json_all_meetings_res = json.loads(all_meetings)
    all_meetings_of_this_user = []
    for meeting in json_all_meetings_res:
        if meeting['fields']['user'] == user_id:
            all_meetings_of_this_user.append(meeting['fields'])
    return Response(all_meetings_of_this_user, status=status.HTTP_200_OK)
