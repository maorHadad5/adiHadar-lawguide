from django.urls import path
from . import views

urlpatterns = [
    path('get_all_lawyers', views.get_all_lawyers),
    path('get_all_users', views.get_all_users),
    path('get_lawyers_by_occupation/<occupation>/', views.get_lawyer_by_occupation),
    path('get_all_occupations/', views.get_all_occupations),
    path('get_all_free_hours_of_lawyer/<int:id_lawyer>/', views.get_all_free_hours_of_lawyer),
    path('get_the_waiting_for_lawyer_confirm_meetings/<int:lawyer_id>/', views.get_the_waiting_for_lawyer_confirm_meetings),
    path('add_meeting/', views.add_meeting),
    path('confirm_meeting/<int:meeting_id>/', views.confirm_meeting),
    path('get_all_meetings_of_lawyer/<int:lawyer_id>/', views.get_all_meetings_of_lawyer),
    path('get_all_meetings_of_user/<int:user_id>/', views.get_all_meetings_of_user),
    path('get_all_recommendations/', views.get_all_recommendations),
    path('get_confirmed_meetings/<int:lawyer_id>/', views.get_confirmed_meetings),
    path('get_url_by_id_room/<int:id_room>/', views.get_url_by_id_room),
    path('get_user_by_id/<int:user_id>/', views.get_user_by_id),
    path('get_lawyer_by_id/<int:lawyer_id>/', views.get_lawyer_by_id),

]