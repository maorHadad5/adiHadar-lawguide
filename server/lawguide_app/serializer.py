from rest_framework import serializers

from .models import User,Lawyer,Occupation


class lawguide_app_Serializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ['id', 'name']

    class Meta:
        model = Lawyer
        fields = ['name', 'location', 'id_lawyer']

    class Meta:
        model = Occupation
        fields = ['id', 'occupation']