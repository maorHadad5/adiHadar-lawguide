from django.apps import AppConfig


class LawguideAppConfig(AppConfig):
    name = 'lawguide_app'
