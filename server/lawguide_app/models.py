from django.db import models


class User(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=200)


class Lawyer(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=200)
    location = models.CharField(max_length=200)
    phone_number = models.CharField(max_length=200)
    free_text_about_the_lawyer = models.CharField(max_length=400)
    office_name = models.CharField(max_length=400)


class Occupation(models.Model):
    id = models.AutoField(primary_key=True)
    id_lawyer = models.ManyToManyField(Lawyer, related_name='lawyers')
    occupation = models.CharField(max_length=200)


class Hours(models.Model):
    id = models.AutoField(primary_key=True)
    hour = models.CharField(max_length=200)


class Meetings(models.Model):
    id = models.AutoField(primary_key=True)
    lawyer = models.ForeignKey(Lawyer, on_delete=models.CASCADE)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    hour = models.CharField(max_length=200)
    waiting_for_lawyer_confirm = models.BooleanField(default=False)
    confirmed = models.BooleanField(default=False)


class Recommendations(models.Model):
    id = models.AutoField(primary_key=True)
    picture = models.CharField(max_length=2000)
    text = models.CharField(max_length=200)

class VideoRooms(models.Model):
    id = models.AutoField(primary_key=True)
    url = models.CharField(max_length=2000)
    taken = models.BooleanField(default=False)